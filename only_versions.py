# This file checks how many CVEs have only CVSS v1 scores, cvss v2 scores, and CVSS v3 scores. 
import json


def read_jsonl(path):
    """
    Reads a jsonl file and returns a list of dictionaries.
    """
    with open(path, 'r') as f:
        return [json.loads(line) for line in f]


data = read_jsonl('nvd_crawled_combined.jsonl')

cves=[d['result']['CVE_Items'] for d in data]
v1s=[]
v2s=[]
v3s=[]
others=[]
for cve in cves:
    for entry in cve:
        # check if baseMetricV2 exists and other scores don't
        if 'baseMetricV2' in entry['impact'] and 'baseMetricV3' not in entry['impact']:
            v2s.append(entry)
        elif 'baseMetricV3' in entry['impact'] and 'baseMetricV2' not in entry['impact']:
            v3s.append(entry)
        elif 'baseMetricV3' not in entry['impact'] and 'baseMetricV2' not in entry['impact']:
            others.append(entry)

def save_data_as_jsonl(data, path):
    with open(path, 'w') as f:
        for d in data:
            f.write(json.dumps(d) + '\n')

save_data_as_jsonl(v2s,'version_wise_data/cvss2.jsonl')
save_data_as_jsonl(v3s,'version_wise_data/cvss3.jsonl')
save_data_as_jsonl(others,'version_wise_data/other.jsonl')