import json


def read_jsonl(path):
    """
    Reads a jsonl file and returns a list of dictionaries.
    """
    with open(path, 'r') as f:
        return [json.loads(line) for line in f]

CWE='NVD-CWE-Other'

data = read_jsonl('cwe_cvss2.jsonl')
# Find data with given cwe
def find_cwe(data, cwe):
    """
    Returns a list of dictionaries containing the given cwe.
    """
    return [d for d in data if d['cwe'] == cwe]

result=(find_cwe(data, CWE))

# save jsonl file
def save_data_as_jsonl(data, path):
    with open(path, 'w') as f:
        for d in data:
            f.write(json.dumps(d) + '\n')

save_data_as_jsonl(result,CWE+'.jsonl')