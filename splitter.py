# Split the CVEs into MITRE managed ones and CNA managed ones
import json


DIVISION_DATE="2021-04-02"
def read_jsonl(file):
    with open(file, 'r') as f:
        return [json.loads(line) for line in f.readlines()]

results=read_jsonl('nvd_crawled_combined.jsonl')
# split the data according to the division date
data_before=[]
data_after=[]
for result in results:
    CVES=result['result']['CVE_Items']
    for CVE in CVES:
        publishedDate=CVE['publishedDate']
        if publishedDate<=DIVISION_DATE:
            data_before.append(CVE)
        else:
            data_after.append(CVE)
print(len(data_before))
print(len(data_after))

def save_jsonl(path,data):
    with open(path,'w') as f:
        for d in data:
            f.write(json.dumps(d)+'\n')

save_jsonl('2021/data_before.jsonl',data_before)
save_jsonl('2021/data_after.jsonl',data_after)