'''
Count the number of CVEs with no CVSS score.
'''

import json


def read_jsonl(path):
    """
    Reads a jsonl file and returns a list of dictionaries.
    """
    with open(path, 'r') as f:
        return [json.loads(line) for line in f]


data = read_jsonl('nvd_crawled_1645731298893.jsonl')


def count_no_cvss_score(data):
    """
    Count the number of CVEs with no CVSS score.
    """
    count = 0
    cves = []
    for result in data:
        for item in result['result']['CVE_Items']:
            if item['impact'] == {}:
                count += 1
                cves.append(item)
    return count, cves


def write_jsonl(path, data):
    """
    Writes a jsonl file.
    """
    with open(path, 'w') as f:
        for item in data:
            f.write(json.dumps(item) + '\n')


count, cves = (count_no_cvss_score(data))
write_jsonl('cves_no_cvss_score.jsonl', cves)
