# Combine multiple jsonl files into one
import json
import os


def read_jsonl(file):
    with open(file, 'r') as f:
        return [json.loads(line) for line in f.readlines()]

def write_jsonl(data, path):
    with open(path, 'w') as f:
        for d in data:
            f.write(json.dumps(d) + '\n')

# read all jsonl files start with nvd_crawled_
files = [f for f in os.listdir('.') if os.path.isfile(f) and f.startswith('nvd_crawled_')]
print(files)
data = []
for f in files:
    print(f)
    data.extend(read_jsonl(f))
write_jsonl(data, 'nvd_crawled_combined.jsonl')