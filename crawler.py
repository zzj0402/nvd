import json
import time
from urllib import response
import requests

API_KEY=''

def get_nvd_data(start, length):
    r = requests.get(url="https://services.nvd.nist.gov/rest/json/cves/1.0",
                     params={"startIndex": start, "resultsPerPage": length},
                     headers={"api_key": API_KEY})
    return r.json()


def write_jsonl(data, path):
    with open(path, 'a') as f:
        f.write(json.dumps(data) + '\n')


def store_batch_start(start, file):
    with open(file, 'w') as f:
        f.write(str(start))


def load_batch_start(file):
    with open(file, 'r') as f:
        return int(f.read())


def get_timestamp():
    return int(round(time.time() * 1000))


def crawl():
    try:
        result = get_nvd_data(0, 1)
        time_stamp = str(get_timestamp())
        BATCH_START_FILE = 'batch_start.txt'
        start = load_batch_start(BATCH_START_FILE)
        BATCH_START = start//100
        total = result['totalResults'] - start
        for i in range(total // 100 + 1):
            response = get_nvd_data(start, 100)
            print(f"{i+1}/{total // 100 + 1}")
            write_jsonl(response, "nvd_crawled_"+time_stamp+".jsonl")
            start += 100
            store_batch_start(start, BATCH_START_FILE)
            print("sleeping for 9 seconds")
            time.sleep(9)
    except Exception:
        print("sleeping for 9 seconds")
        time.sleep(9)
        print("Exception occurred, restarting crawling")
        crawl()
crawl()