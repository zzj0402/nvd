from sklearn.model_selection import train_test_split
import json


def read_jsonl(path):
    with open(path, 'r') as f:
        for line in f:
            yield json.loads(line)


data = list(read_jsonl('nvd_crawled_1645731298893.jsonl'))


def get_metric(data, version='baseMetricV2'):
    """
    Returns a list containing the impact of the data.
    """
    impacts = []
    for result in data:
        for item in result['result']['CVE_Items']:
            if version in item['impact']:
                impacts.append(item['impact'][version])
    return impacts


def count_component_labels(component, metrics):
    labels = []
    result = {}
    for metric in metrics:
        labels.append(metric['cvssV2'][component])
    kinds = set(labels)
    for k in kinds:
        result[k] = labels.count(k)
    return result


def get_dataset_total(component):
    metrics = get_metric(data)
    stats = count_component_labels(component, metrics)
    return stats


def resample():
    sample_state = 0
    train, test = train_test_split(
        data, test_size=0.3, random_state=sample_state)
    CVSS_VERSION = 'baseMetricV2'
    metrics = get_metric(test, CVSS_VERSION)

    compoents = ['accessVector', 'accessComplexity', 'authentication',
                 'confidentialityImpact', 'integrityImpact', 'availabilityImpact']
    data_stats = {}
    for component in compoents:
        data_stats[component] = count_component_labels(component, metrics)
    for component in data_stats:
        class_amount = len(data_stats[component].keys())
        print(component, data_stats[component])
        if class_amount != 3:
            sample_state += 1
            print('Insufficient labels, resampling...')
            resample()
        total_stats = get_dataset_total(component)
        for key in data_stats[component].keys():
            test_label_percentage = data_stats[component][key] / \
                total_stats[key]
            print(component, key, test_label_percentage)
            if test_label_percentage < 0.1:
                sample_state += 1
                print('Insufficient data for {}'.format(component))
                print('Resampling...')
                resample()
    with open('train.json', 'w') as f:
        json.dump(train, f)
    with open('test.json', 'w') as f:
        json.dump(test, f)
    with open('testdata_states.json', 'w') as f:
        json.dump(data_stats, f)


resample()
