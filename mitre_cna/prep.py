# process the data into one CVE per line
import json


def read_jsonl(file):
    with open(file, 'r') as f:
        return [json.loads(line) for line in f.readlines()]

data=read_jsonl('mitre_cna/cna_managed.jsonl')
print(len(data))