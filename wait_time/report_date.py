from pathlib import Path
import json
from datetime import datetime
result=[]
for path in Path('wait_time/cvelistV5-main/cves').rglob('*.json'):
    try:
        cve = json.load(open(path, 'r', encoding='cp932', errors='ignore'))
        if cve['cveMetadata']['dateReserved'] == None:
            continue
        date_reserved = cve['cveMetadata']['dateReserved'][0:10]
        print(date_reserved)
        id = cve['cveMetadata']['cveId']
        print(id)
        result.append({"id":id,"date_reserved":date_reserved})
    except:
        continue
def write_jsonl(file, data):
    with open(file, 'w') as f:
        for d in data:
            f.write(json.dumps(d) + '\n')

write_jsonl('wait_time/cve_reserved_date.jsonl',result)