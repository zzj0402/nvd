import json


def read_jsonl(file):
    with open(file, 'r') as f:
        return [json.loads(line) for line in f.readlines()]

data=read_jsonl('wait_time/diff.jsonl')
no_cvss=[d for d in data if d<=-1]
smallest=min(data)
# remove negative values
data=[d for d in data if d>=0]
# count the number of waiting time
from collections import Counter
counts=Counter(data)
print(counts)
print('Total Number of CVEs with CVSS in the Crawled Dataset: ',len(data))
# longest waiting time
print('Longest Waiting Days: ',max(data))
# shortest waiting time
print('Shortest Waiting Days: ',min(data))
# how many of them are 0
print('Number of CVEs with 0 Waiting Days: ',counts[0])
print('Percentage of CVEs with 0 Waiting Days: ',counts[0]/len(data))
# average waiting time
print('Average Waiting Days: ',sum(data)/len(data))
# how many of them are above 124 days waiting time
print('Number of CVEs with Waiting Days > 124: ',sum([1 for d in data if d>=124]))
# percentage of them are above 124 days waiting time
print('Percentage of CVEs with Waiting Days > 124: ',sum([1 for d in data if d>=124])/len(data))
# median waiting time
print('Median Waiting Days: ',sorted(data)[len(data)//2])
# how many of them are above 48 days waiting time
print('Number of CVEs with Waiting Days > 48: ',sum([1 for d in data if d>=48]))
# most frequent waiting time
print('Most Frequent Waiting Days: ',counts.most_common(1))
# percentage of them are above 48 days waiting time
print('Percentage of CVEs with Waiting Days > 48: ',sum([1 for d in data if d>=48])/len(data))
print('Number of CVEs without CVSS: ',len(no_cvss))
# smallest value in the data
print('Longest CVE without CVSS: ',smallest)