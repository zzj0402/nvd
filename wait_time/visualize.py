import json


def read_jsonl(file):
    with open(file, 'r') as f:
        return [json.loads(line) for line in f.readlines()]

data=read_jsonl('wait_time/diff.jsonl')
# remove negative values
data=[d for d in data if d>=0]

# visulize the counted data
import matplotlib.pyplot as plt
plt.hist(data,bins=[0,10,30,40,6000])
plt.savefig('wait_time/waiting_time_distribution.png')