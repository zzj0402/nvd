# combine two json line files by CVE IDs
import json


def read_jsonl(file):
    with open(file, 'r') as f:
        return [json.loads(line) for line in f.readlines()]

reserved_date=read_jsonl('wait_time/cve_reserved_date.jsonl')
publish_date=read_jsonl('wait_time/cvss_publish_date.jsonl')
result=[{"id":r['id'],"reserved_date":r['date_reserved'],"publish_date":p['publishedDate'][0:10]} for r in reserved_date for p in publish_date if r['id']==p['id']]
print(result[0])
# calculate the difference by days
from datetime import datetime
def date_diff(date1,date2):
    date1=datetime.strptime(date1,'%Y-%m-%d')
    date2=datetime.strptime(date2,'%Y-%m-%d')
    return (date2-date1).days
diffs=[date_diff(r['reserved_date'],r['publish_date']) for r in result]
# save the diffs
def save_jsonl():
    with open('wait_time/diff.jsonl','w') as f:
        for d in diffs:
            f.write(json.dumps(d)+'\n')
save_jsonl()
