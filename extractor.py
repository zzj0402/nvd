# Extract
import json


def read_jsonl(path):
    """
    Reads a jsonl file and returns a list of dictionaries.
    """
    with open(path, 'r') as f:
        return [json.loads(line) for line in f]


data = read_jsonl('nvd_crawled_1645731298893.jsonl')

cves=[d['result']['CVE_Items'] for d in data]
cvss2s=[]
for cve in cves:
    for entry in cve:
        # check if baseMetricV2 exists
        if 'baseMetricV2' in entry['impact']:
            score=(entry['impact']['baseMetricV2'])
            datum={'cve':entry['cve'],'impact':score}
            cvss2s.append(datum)
            
def save_data_as_jsonl(data, path):
    """
    Saves a list of dictionaries to a jsonl file.
    """
    with open(path, 'w') as f:
        for d in data:
            f.write(json.dumps(d) + '\n')

save_data_as_jsonl(cvss2s,'cvss2.jsonl')
