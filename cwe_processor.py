import json


def read_jsonl(path):
    """
    Reads a jsonl file and returns a list of dictionaries.
    """
    with open(path, 'r') as f:
        return [json.loads(line) for line in f]


data = read_jsonl('cvss2.jsonl')

# Extract problem types
output=[]
for d in data:
    # print(d)
    cwe = d['cve']['problemtype']['problemtype_data'][0]['description']
    cwe=cwe[0]['value'] if len(cwe)>0 else 'None'
    cvss=d['impact']['cvssV2']['baseScore']
    description=d['cve']['description']['description_data'][0]['value']
    output.append({'description':description,'cwe':cwe,'cvss':cvss})

def save_outputs_jsonl(output,path):
    with open(path,'w') as f:
        for o in output:
            f.write(json.dumps(o)+'\n')

save_outputs_jsonl(output,'cwe_cvss2.jsonl')