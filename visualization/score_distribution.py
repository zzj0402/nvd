import json


def read_jsonl(file):
    with open(file, 'r') as f:
        return [json.loads(line) for line in f.readlines()]

results=read_jsonl('nvd_crawled_combined.jsonl')
output=[]
for result in results:
    CVES=result['result']['CVE_Items']
    for CVE in CVES:
        if 'baseMetricV3' in CVE['impact']:
            score=CVE['impact']['baseMetricV3']['cvssV3']['baseScore']
            output.append(score)

# Count the number of scores in each interval
import collections
counter=collections.Counter(output)
print(counter)
null_values=[]
for i in range(0,100,1):
    cvss_value=float(i/10)
    if cvss_value not in counter.keys():
        null_values.append(cvss_value)
print(null_values)
# visulize the scores in an interval of 0.1
import matplotlib.pyplot as plt
plt.hist(output,bins=100)
plt.savefig('visualization/cvss_score_v3_distribution_100.png')