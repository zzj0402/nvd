# read jsonl data
import json

def read_jsonl(file):
    with open(file, 'r') as f:
        return [json.loads(line) for line in f.readlines()]

data=read_jsonl('modified_date/cvss_modified_date.jsonl')


# count the number of CVEs by month
from collections import Counter
def count_month(data):
    months=[d['modifiedDate'][0:7] for d in data]
    return Counter(months)
month_counts=(count_month(data))
print("Month Counts:")
print(month_counts)
# count the years instead of months
def count_year(data):
    years=[d['modifiedDate'][0:4] for d in data]
    return Counter(years)
year_counts=(count_year(data))
print("Year Counts:")
print(year_counts)
# visulize the counted data
# import matplotlib.pyplot as plt
# plt.bar(month_counts.keys(),month_counts.values())
# plt.savefig('modified_date/modified_date_month_count.png')
# plt.bar(year_counts.keys(),year_counts.values())
# plt.savefig('modified_date/modified_date_year_count.png')
