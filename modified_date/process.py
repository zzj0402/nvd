import json


def read_jsonl(file):
    with open(file, 'r') as f:
        return [json.loads(line) for line in f.readlines()]

results=read_jsonl('nvd_crawled_combined.jsonl')
output=[]
for result in results:
    CVES=result['result']['CVE_Items']
    for CVE in CVES:
        id=CVE['cve']['CVE_data_meta']['ID']
        publishedDate=CVE['publishedDate']
        modifiedDate=CVE['lastModifiedDate']
        output.append({"id":id,"publishedDate":publishedDate,"modifiedDate":modifiedDate})

def save_jsonl(path,data):
    with open(path,'w') as f:
        for d in data:
            f.write(json.dumps(d)+'\n')

save_jsonl('modified_date/cvss_modified_date.jsonl',output)
