import json


def read_jsonl(path):
    """
    Reads a jsonl file and returns a list of dictionaries.
    """
    with open(path, 'r') as f:
        return [json.loads(line) for line in f]


data = read_jsonl('nvd_crawled_1645731298893.jsonl')


def most(lst):
    """
    Returns the most frequent element in a list.
    """
    return max(set(lst), key=lst.count)


def get_metric(data, version='baseMetricV2'):
    """
    Returns a list containing the impact of the data.
    """
    impacts = []
    for result in data:
        for item in result['result']['CVE_Items']:
            if version in item['impact']:
                impacts.append(item['impact'][version])
    return impacts


CVSS_VERSION = 'baseMetricV2'
metrics = get_metric(data, CVSS_VERSION)
availability_impacts = [metrics[i]['cvssV2']['availabilityImpact']
                        for i in range(len(metrics))]

print('Most Frequent:', most(availability_impacts), "Occurrences:",
      availability_impacts.count(most(availability_impacts)))
print('Percentage:', round(availability_impacts.count(
    most(availability_impacts)) / len(availability_impacts) * 100, 2), '%')
