import matplotlib.pyplot as plt
import json


def read_jsonl(path):
    """
    Reads a jsonl file and returns a list of dictionaries.
    """
    with open(path, 'r') as f:
        return [json.loads(line) for line in f]


data = read_jsonl('cwe_cvss2.jsonl')


def get_length_distribution(data, max, min):
    """
    Returns a list containing the length of the data.
    """
    impactScore = []
    max_description = []
    min_description = []
    for d in data:
        result = d['result']
        for item in result['CVE_Items']:
            description = item['cve']['description']['description_data'][0]['value']
            length = len(description)
            if length == max:
                max_description.append(description)
            if length == min:
                min_description.append(description)
            impactScore.append(length)
    return impactScore, max_description, min_description


def most(lst):
    """
    Returns the most frequent element in a list.
    """
    return max(set(lst), key=lst.count)


def least(lst):
    """
    Returns the least frequent element in a list.
    """
    return min(set(lst), key=lst.count)


def get_metric(data, version='baseMetricV2'):
    """
    Returns a list containing the impactScore of the data.
    """
    impactScore = []

    for result in data:
        for item in result['result']['CVE_Items']:
            if version in item['impact']:
                impactScore.append(item['impact'][version])
    return impactScore


def get_impactScore(data):
    """
    Returns a list containing the impactScore of the data.
    """
    impactScore = []

    for result in data:
        for item in result['result']['CVE_Items']:
            if 'baseMetricV2' in item['impact']:
                impactScore.append(
                    item['impact']['baseMetricV2']['impactScore'])

    return impactScore


# impactScore = get_impactScore(data)

# print('Max:', max(impactScore), "Occurrences:",
#       impactScore.count(max(impactScore)))
# print('Min:', min(impactScore), "Occurrences:",
#       impactScore.count(min(impactScore)))
# print('Most Frequent:', most(impactScore),
#       "Occurrences:", impactScore.count(most(impactScore)))
# print('Least Frequent:', least(impactScore),
#       "Occurrences:", impactScore.count(least(impactScore)))
# impactScore.sort()
# plt.ylabel('Amount')
# plt.xlabel('impactScore')
# plt.title('Distribution of impactScore')
# plt.hist(impactScore)
# plt.savefig('impactScore_distribution.png')
print(data[0])
CWES=[d['cwe'] for d in data]
# Couting the number of occurrences of each CWE
def count_cwe(data):
    """
    Returns a dictionary containing the number of occurrences of each CWE.
    """
    cwe = {}
    for d in data:
        if d['cwe'] in cwe:
            cwe[d['cwe']] += 1
        else:
            cwe[d['cwe']] = 1
    # sort the dictionary by occurrences
    cwe = {k: v for k, v in sorted(cwe.items(), key=lambda item: item[1])}
    return cwe

print(count_cwe(data))